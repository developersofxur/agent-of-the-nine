import asyncio
from datetime import datetime, timedelta, timezone
from tokenize import Number
import discord
from discord.ext import commands

XUR_HUNTER_CHAN = 518150211357507588


def queue_func(cog: commands.Cog, offset: int, func):
    async def sleep_wrapper():
        await cog.bot.wait_until_ready()
        while not cog.bot.is_closed():
            now = datetime.now(timezone.utc)
            friday_reset = datetime(now.year, now.month, now.day, 17, 0, 0, 0, timezone.utc) + \
                timedelta((4 - now.weekday()) % 7) + timedelta(seconds=offset)
            if now.weekday() == friday_reset.weekday() and now > friday_reset:
                friday_reset += timedelta(7)
            print(friday_reset)
            delta = friday_reset - now
            await asyncio.sleep(delta.total_seconds())
            try:
                await func()
            except:
                print(f"Error with function at {friday_reset}")
            await asyncio.sleep(1)
    cog.bot.loop.create_task(sleep_wrapper())


class XurResetMessenger(commands.Cog):
    def __init__(self, bot):
        self.bot: commands.Bot = bot
        # queue_func(self, -90 * 60, self.early_warning)
        # queue_func(self, -10 * 60, self.planet_claim)
        # queue_func(self, 0, self.gogogo)

    async def early_warning(self):
        chan = await self.bot.fetch_channel(XUR_HUNTER_CHAN)
        await chan.send("<@&518149972340768768>\n90 minute warning!")
        print("90 min warning")

    async def planet_claim(self):
        chan = await self.bot.fetch_channel(XUR_HUNTER_CHAN)
        msg: discord.Message = await chan.send("<@&518149972340768768>\nIts almost Xur time!  Go ahead and react to the planet you'll be checking, try to make sure there's none left over!  Also wait until reset has happened in approximately 5 minutes before loading into the area you're checking!\n\n<:traveler:520637103776989195>: Tower > The Hangar > Above Dead Orbit\n<:nessus:520637103285993473>: Nessus > Watcher's Grave > On his barge\n<:earth:520637103927853059>: Earth (EDZ) > Winding Cove > On the cliff")
        await msg.add_reaction(self.bot.get_emoji(520637103776989195))
        await msg.add_reaction(self.bot.get_emoji(520637103285993473))
        await msg.add_reaction(self.bot.get_emoji(520637103927853059))
        print("10 min warning")

    async def gogogo(self):
        chan = await self.bot.fetch_channel(XUR_HUNTER_CHAN)
        await chan.send("<@&518149972340768768>\nGOGOGOGOGOGOGOGO")
        print("gogogo")


def setup(bot):
    bot.add_cog(XurResetMessenger(bot))
