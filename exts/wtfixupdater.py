import json
from datetime import datetime, timezone
from typing import List

import discord
from discord.ext import commands

import firebase_admin
from firebase_admin import db

import markdown

XUR_LOCS = {
    'tower': {
        'planet': 'Tower',
        'zone': 'The Hangar',
        'desc': 'Behind Dead Orbit',
        'img': '/images/tower_map_light.png'
    },
    'nessus': {
        'planet': 'Nessus',
        'zone': "Watcher's Grave",
        'desc': 'In his tree',
        'img': '/images/nessus_map_light.png'
    },
    'earth': {
        'planet': "Earth",
        'zone': 'Winding Cove', 
        'desc': 'On his cliff',
        'img': '/images/earth_map_light.png'
    },
    'edz': {
        'planet': "Earth",
        'zone': "Winding Cove",
        'desc': 'On his cliff',
        'img': '/images/earth_map_light.png'
    }
}


class WTFIXUpdaterCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        firebase_admin.initialize_app(options={
            'databaseURL': 'https://where-the-fuck-is-xur-ed4bd.firebaseio.com/'
        })
        self.xurRef = db.reference('xur')
        self.msgRef = db.reference('msg')

    @commands.command()
    @commands.has_any_role(502728691089014784)
    async def xur(self, ctx: commands.Context, *, arg: str):
        id = arg.lower()
        if isXurHere():
            if id in XUR_LOCS:
                xur = XUR_LOCS[id]
                xur['found'] = True
                xur['present'] = True
                xur['lastUpdate'] = datetime.now().timestamp() * 1000
                self.xurRef.set(xur)
                await ctx.send("Updated!")
            else:
                await ctx.send(
                    "That's not a valid location, please use `tower`, `titan`, `io`, `nessus`, or `edz`!")
        else:
            await ctx.send("Xur isn't here, so you can't update his location!")

    @commands.command()
    @commands.has_any_role(502728691089014784)
    async def xurcustom(self, ctx: commands.Context, *, arg: str):
        if isXurHere():
            texts = [x.strip() for x in arg.split("|")]
            if len(texts) >= 3:
                xur = {}
                xur['planet'] = texts[0]
                xur['zone'] = texts[1]
                xur['desc'] = texts[2]
                xur['img'] = "https://wherethefuckisxur.com/images/loc_unavailable.png"
                xur['found'] = True
                xur['present'] = True
                xur['lastUpdate'] = datetime.now().timestamp() * 1000
                self.xurRef.set(xur)
                await ctx.send("Updated!")
            else:
                await ctx.send("Not enough info, please provide xur's planet, zone, and description separated by these characters: |")
        else:
            await ctx.send("Xur isn't here, so you can't update his location!")

    @commands.command()
    @commands.has_any_role(502728691089014784)
    async def xurimg(self, ctx: commands.Context, *, arg: str):
        url = arg.strip()
        self.xurRef.update({'img': url})
        await ctx.send("Updated!")

    @commands.command(rest_is_raw=True)
    @commands.has_any_role(502728691089014784)
    async def xurmsg(self, ctx: commands.Context, *, arg: str):
        await self.update_msg(ctx, arg, 'xurmsg')

    @commands.command(rest_is_raw=True)
    @commands.has_any_role(502728691089014784)
    async def psa(self, ctx: commands.Context, *, arg: str):
        await self.update_msg(ctx, arg, 'psa')

    @commands.command(rest_is_raw=True)
    @commands.has_any_role(502728691089014784)
    async def riff(self, ctx: commands.Context, *, arg: str):
        await self.update_msg(ctx, arg, 'riff')

    async def update_msg(self, ctx: commands.Context, arg: str, field: str):
        content = markdown.markdown(arg)
        self.msgRef.child(field).set(content)
        await ctx.send(f"Updated the {field} to say ```{content}```")


def setup(bot):
    bot.add_cog(WTFIXUpdaterCog(bot))

def isXurHere():
    now = datetime.now(timezone.utc)
    if now.weekday() > 4 or now.weekday() < 1:
        return True
    elif now.weekday() == 4 and now.hour >= 17:
        return True
    elif now.weekday() == 1 and now.hour < 17:
        return True
    else:
        return False